# Templates

This is a repository for template files. These can be used as starting points for various forms of artifacts such as articles, documents, forms, licenses, etc. 


## Software Development

- [ADR-Template.md](https://gitlab.com/algonzalez/templates/blob/master/ADR-Template.md): an Architecture Decision Record template, as described in Michael Nygard's blog post [Documenting Architecture Decisions](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).
- [CHANGELOG-Template.md](https://gitlab.com/algonzalez/templates/blob/master/CHANGELOG-Template.md): project version change log template to maintain a curated, chronologically ordered list of changes for each project version. Attempts to follow the guidelines described at the [Keep a Changelog](http://keepachangelog.com/) web site.
- TODO: 
    - apache license
    - mit license
    - copyright (source code file header)
    - .editorconfig
    - .gitignore (for various projects)
    - .bash_rc/.bash_profile/.profile 
    - (??? dotfiles repo instead ???)
    - ...


## Writing

- TODO:

----

URLs
- Project: https://gitlab.com/algonzalez/templates
- Public Git Repository: https://gitlab.com/algonzalez/templates.git
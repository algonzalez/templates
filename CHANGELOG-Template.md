# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
Please see the [Keep a Change Log](http://keepachangelog.com/) site for details about the structure of this file.

## [Unreleased]

### Added
- No new features.

### Changed
- No changes to existing functionality.

### Fixed
- No bugs have been fixed.

### Deprecated
- No features have been marked for removal in upcoming releases.

### Removed
- No previously deprecated features have been removed in this release.

### Security
- No security vulnerabilities have been addressed in this release.

### Known Issues
- No known side effects or issues to report for this release.


## [0.1.0] - YYYY-MM-DD

### Added
- initial public beta

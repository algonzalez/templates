# Architecture Decision Record (ADR) Title

Title Examples:
- ADR 1: Deployment on Spark Java web micro-framework
- ADR 9: Cloud Service for Sending Emails

**_NOTE:_** descriptions of the sections of an ADR have been lifted and possibly paraphrased from the following Michael Nygard article: http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions

## Context

Describes the challenges and motivations, including technological, political, social and project. We will describe the facts in a value-neutral style.

## Decision

Describes our response to the challenges and motivations listed in the Context section. We will use full sentences and an active voice (ex: "We Will ...").

Should also, include any alternatives that were considered and why they were not chosen.

## Status

One of the following:

- Proposed {pending approval}
- Accepted on YYYY-MM-DD
- Deprecated on YYYY-MM-DD
- Superceded by ADR-NNN on YYYY-MM-DD

**_NOTES:_**
- dates may not be required if ADRs are maintained in source control
- transitional status changes (ie. Deprecated and Superceded) will be elaborated upon in the Decision and Consequences sections.

## Consequences

Describes the resulting context, after applying the decision. All consequences should be listed here, not just positive ones. A decision may have positive, negative and neutral consequences, but all of them affect the team and project in the future.

The whole document should be one or two pages long. We will write each ADR as if it is a conversation with a future developer. This requires good writing style, with full sentences organized into paragraphs. Bullets are acceptable only for visual style, not as an excuse for writing sentence fragments. (Bullets kill people, even PowerPoint bullets.)

---
### Recommendations for using ADRs:

- use Markdown for formmatting document.
- name files using the adr-NNN_TitleText.md format (may omit the TitleText if desired).
- store in project repository under docs/arch/adr-NNN.md under project files
- number sequentially from 1. So, first ADR will be named adr-001.md
- keep all ADRs even, if reversed. Change status to Deprecated or Superceded and explain why.
